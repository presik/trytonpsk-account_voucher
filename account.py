# This file is part of Tryton.  The COPYRIGHT file at the top level of
# this repository contains the full copyright notices and license terms.
from trytond.model import MatchMixin, ModelSQL, ModelView, fields, sequence_ordered
from trytond.pool import PoolMeta
from trytond.pyson import Eval, Id
from trytond.transaction import Transaction


class Move(metaclass=PoolMeta):
    __name__ = 'account.move'

    @classmethod
    def _get_origin(cls):
        return super(Move, cls)._get_origin() + ['account.voucher'] + ['account.note']


class MoveLine(metaclass=PoolMeta):
    __name__ = 'account.move.line'

    @classmethod
    def _get_origin(cls):
        return super(MoveLine, cls)._get_origin() + ['account.voucher.line', 'account.note.line']


class VoucherSequencePeriod(sequence_ordered(), ModelSQL, ModelView, MatchMixin):
    "Voucher Sequence Period"
    __name__ = 'account.voucher.sequence_period'
    company = fields.Many2One('company.company', 'Company', required=True)
    fiscalyear = fields.Many2One(
        'account.fiscalyear', "Fiscal Year", required=True, ondelete='CASCADE',
        domain=[('company', '=', Eval('company', -1))],
        depends=['company'])
    period = fields.Many2One('account.period', 'Period', domain=[
        ('company', '=', Eval('company', -1)),
        ])
    payment_sequence = fields.Many2One('ir.sequence',
        'Voucher Payment Sequence', required=False, domain=[
            ('company', 'in', [Eval('company', -1), None]),
            ('sequence_type', '=',
                Id('account_voucher', 'sequence_type_voucher')),
            ])
    receipt_sequence = fields.Many2One('ir.sequence',
        'Voucher Receipt Sequence', required=False,
        domain=[
            ('company', 'in', [Eval('company', -1), None]),
            ('sequence_type', '=',
                Id('account_voucher', 'sequence_type_voucher')),
        ])
    notes_sequence = fields.Many2One('ir.sequence',
        'Voucher Notes Sequence', required=False,
        domain=[
            ('company', 'in', [Eval('company', -1), None]),
            ('sequence_type', '=',
                Id('account_voucher', 'sequence_type_voucher')),
        ])
    # multirevenue_sequence = fields.Many2One('ir.sequence',
    #     'Multi-Revenue Sequence', required=False,
    #     domain=[
    #         ('company', 'in', [Eval('company', -1), None]),
    #         ('code', '=', 'account.multirevenue')
    #     ])
    multipayment_sequence = fields.Many2One('ir.sequence',
        'Multi-Payment Sequence', required=False,
        domain=[
            ('company', 'in', [Eval('company', -1), None]),
            ('sequence_type', '=',
                Id('account_voucher', 'sequence_type_voucher')),
        ])

    @classmethod
    def __setup__(cls):
        super(VoucherSequencePeriod, cls).__setup__()
        cls._order.insert(0, ('fiscalyear', 'ASC'))

    @classmethod
    def default_company(cls):
        return Transaction().context.get('company')


class FiscalYear(metaclass=PoolMeta):
    __name__ = 'account.fiscalyear'
    voucher_sequences = fields.One2Many(
        'account.voucher.sequence_period', 'fiscalyear',
        "Voucher Sequences",
        domain=[('company', '=', Eval('company', -1))],
        depends=['company'])


class PayLine(metaclass=PoolMeta):
    "Pay Line"
    __name__ = 'account.move.line.pay'

    def get_payment(self, line, journals):
        payment = super(PayLine, self).get_payment(line, journals)
        if line.move_origin:
            payment.origin = str(line.move_origin)
            if line.move_origin.__name__ == 'account.invoice' \
                and line.account.id == line.move_origin.account.id \
                and line.move_origin.payment_lines:

                amount_paid = sum(abs(p.debit - p.credit) for p in line.move_origin.payment_lines)
                if amount_paid > 0:
                    payment.amount = payment.amount - amount_paid

        return payment
