# This file is part of Tryton.  The COPYRIGHT file at the top level of
# this repository contains the full copyright notices and license terms.
from trytond.exceptions import UserError
from trytond.i18n import gettext
from trytond.model import ModelSQL, ModelView, Unique, fields
from trytond.pyson import Eval, Id
from trytond.transaction import Transaction


class VoucherConfiguration(ModelSQL, ModelView):
    "Voucher Configuration"
    __name__ = 'account.voucher_configuration'
    company = fields.Many2One('company.company', 'Company', required=True)
    default_journal_note = fields.Many2One('account.journal', 'Default Journal Note')
    default_payment_mode = fields.Many2One('account.voucher.paymode',
        'Default Payment Mode', domain=[
            ('company', 'in', [Eval('company', -1), None]),
        ])
    account_adjust_expense = fields.Many2One('account.account',
        'Account Adjustment Zero for Expense', domain=[
            ('type', '!=', None),
        ])
    account_adjust_income = fields.Many2One('account.account',
        'Account Adjustment Zero for Income', domain=[
            ('type', '!=', None),
        ])
    voucher_notes_sequence = fields.Many2One('ir.sequence',
        'Voucher Notes Sequence', required=False,
        domain=[
            ('company', 'in', [Eval('company', -1), None]),
            ('sequence_type', '=',
                Id('account_voucher', 'sequence_type_voucher')),
        ])
    multirevenue_sequence = fields.Many2One('ir.sequence',
        'Multi-Revenue Sequence', required=False,
        domain=[
            ('company', 'in', [Eval('company', -1), None]),
            ('sequence_type', '=',
                Id('account_voucher', 'sequence_type_multirevenue')),
        ])
    customer_advance_account = fields.Many2One('account.account', 'Customer Advance Account',
            domain=[
                ('reconcile', '=', True),
                ('type.statement', 'in', ['balance']),
            ])
    prepayment_description = fields.Char('Prepayment Description',
        help="Default descriptor for advances")
    authorized_users = fields.One2Many(
        'account.voucher_configuration.user_authorized',
        'configuration', 'Authorized Users')
    template_email_confirm = fields.Many2One('email.template',
        'Template Email of Confirmation')

    @classmethod
    def __setup__(cls):
        super(VoucherConfiguration, cls).__setup__()

    @classmethod
    def __register__(cls, module_name):
        table_h = cls.__table_handler__(module_name)

        # rename account_prepayment into customer_advance_account
        if table_h.column_exist('account_prepayment'):
            table_h.column_rename('account_prepayment', 'customer_advance_account')
        super(VoucherConfiguration, cls).__register__(module_name)

    @staticmethod
    def default_company():
        return Transaction().context.get('company')

    @classmethod
    def get_configuration(cls):
        res = cls.search([
            ('company', '=', Transaction().context.get('company')),
        ])
        if res:
            return res[0]
        raise UserError(gettext(
            'account_voucher.msg_missing_default_configuration',
        ))


class VoucherSequence(ModelSQL, ModelView):
    "Voucher Sequence"
    __name__ = 'account.voucher.sequence'
    voucher_receipt_sequence = fields.MultiValue(fields.Many2One('ir.sequence',
        'Voucher Receipt Sequence', required=True,
        domain=[
            ('sequence_type', '=',
                Id('account_voucher', 'sequence_type_voucher')),
        ]))
    voucher_payment_sequence = fields.MultiValue(fields.Many2One('ir.sequence',
        'Voucher Payment Sequence', required=True,
        domain=[
            ('sequence_type', '=',
                Id('account_voucher', 'sequence_type_voucher')),
        ]))
    voucher_notes_sequence = fields.MultiValue(fields.Many2One('ir.sequence',
        'Voucher Notes Sequence', required=True,
        domain=[
            ('sequence_type', '=',
                Id('account_voucher', 'sequence_type_voucher')),
        ]))
    voucher_multipayment_sequence = fields.MultiValue(fields.Many2One('ir.sequence',
        'Voucher Multipayment Sequence', required=True,
        domain=[
            ('sequence_type', '=',
                Id('account_voucher', 'sequence_type_voucher')),
        ]))
    voucher_multirevenue_sequence = fields.MultiValue(fields.Many2One('ir.sequence',
        'Voucher Multirevenue Sequence', required=True,
        domain=[
            ('sequence_type', '=',
                Id('account_voucher', 'sequence_type_multirevenue')),
        ]))


class ResUserAuthorizedVoucher(ModelSQL, ModelView):
    "Res Users - Authorized Voucher "
    __name__ = 'account.voucher_configuration.user_authorized'
    _table = 'voucher_configuration-user_authorized_voucher'
    configuration = fields.Many2One('account.voucher_configuration', 'Configuration',
        ondelete='CASCADE', required=True)
    user = fields.Many2One('res.user', 'User', ondelete='RESTRICT',
        required=True)
    # limit_amount_confirm = fields.Integer("Limit Amount For Confirm")

    @classmethod
    def __setup__(cls):
        super(ResUserAuthorizedVoucher, cls).__setup__()
        table = cls.__table__()

        cls._sql_constraints += [
           ('user_uniq', Unique(table, table.user),
               'User already exists!'),
        ]

    def get_rec_name(self, name):
        return self.user.name
